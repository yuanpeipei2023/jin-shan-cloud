import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { globalMixin } from "./mixins";
import Element from "element-ui";
import DateComponent from "./components/DateTime/date";
import WeekComponent from "./components/DateTime/week";
import TimeComponent from "./components/DateTime/time";
import ScreenFull from "./components/ScreenFull";
import GoPageComponent from "./components/GoPage";
import LineCharts from "./components/LineChart";
import GaugeChart from "./components/GaugeChart";
import ListComponent from "./components/List";
import "element-ui/lib/theme-chalk/index.css";

Vue.config.productionTip = false;
Vue.component("DateComponent", DateComponent);
Vue.component("TimeComponent", TimeComponent);
Vue.component("WeekComponent", WeekComponent);
Vue.component("ScreenFull", ScreenFull);
Vue.component("GoPageComponent", GoPageComponent);
Vue.component("LineCharts", LineCharts);
Vue.component("GaugeChart", GaugeChart);
Vue.component("ListComponent", ListComponent);

Vue.mixin(globalMixin);

Vue.use(Element);

new Vue({
  el: "#app",
  router,
  render: (h) => h(App),
});
