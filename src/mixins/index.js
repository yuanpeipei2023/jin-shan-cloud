export const globalMixin = {
  data() {
    return {
    };
  },
  methods: {
    backgroundStyle(isFlag,backgroundImg_1,backgroundImg_2) {
      return {
        backgroundImage: `url(${
          isFlag ? backgroundImg_1 : backgroundImg_2
        })`,
      };
    },
    colorStyle(value) {
      return {
        color: value.isFlag
          ? `rgba(255, 126, 44, 1)`
          : `rgba(199, 207, 243, 1)`,
      };
    },
    goPage(url) {
      this.$router.push(url);
    },
  }
};