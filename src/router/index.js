import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const constantRoutes = [
  // {
  //   path: '/header',
  //   component: () => import('@/components/DateTime'),
  //   hidden: true,
  //   // children: [
  //   //   {
  //   //     path: '/redirect/:path(.*)',
  //   //     component: () => import('@/views/redirect/index')
  //   //   }
  //   // ]
  // },
  {
    path: "/shujukujiankong",
    component: () => import("@/views/shujukujiankong"),
  },
  {
    path: "/fuwuqijiankong",
    component: () => import("@/views/fuwuqijiankong"),
  },
  {
    path: "/shujuhuiju",
    component: () => import("@/views/shujuhuijushujufuwugongxiangpicijiankong"),
  },
  {
    path: "/shujuhuijujiankong",
    component: () => import("@/views/shujuhuijujiankongduizhangshibai"),
  },
  {
    path: "/table",
    component: () => import("@/views/shujuhuijujiankongtable"),
  },
  {
    path: "/levelwarning",
    component: () => import("@/views/levelWarning"),
  },
  {
    path: "/shouye",
    component: () => import("@/views/shouye"),
  },
];

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    routes: constantRoutes,
  });

const router = createRouter();

export default router;
